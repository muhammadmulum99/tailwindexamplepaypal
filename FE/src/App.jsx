import HeroTextContent from "./components/HeroTextContent";
import ImageContainer from "./components/ImageContainer";
import LeftFooter from "./components/LeftFooter";
import NavIcons from "./components/NavIcons";
import NavLinks from "./components/NavLinks";
import PageCount from "./components/PageCount";
import PayPalPayment from "./components/PayPalPayment";
import RightFooter from "./components/RightFooter";

import {PayPalScriptProvider,PayPalButtons} from "@paypal/react-paypal-js"
function App() {
const initialOptions ={
  "client-id":"AYdKaPaOfm29jVxhhOkZaQtpi8IsPQlmHeCpenbkfsiqz5182DypAi5r0qdr1HrKKg4Jaopd-mK1Dy-x",
  currency:"USD",
  intent:"capture",
  // "data-client-token":""
}

  return (
    <PayPalScriptProvider options={initialOptions}>
    <div className="w-screen h-screen font-alata flex">
      <div className="h-full flex-1 pl-40 pr-24 pb-8 flex flex-col justify-between">
        <NavLinks/>
        <HeroTextContent/>
       <PayPalPayment/>
        <LeftFooter/>
      </div>
      <div className="h-full flex-1 bg-[#FFF0C8] pr-40 pb-8 flex flex-col justify-between relative">
        <NavIcons/>
        <ImageContainer/>
        <PageCount/>
        <RightFooter/>
      </div>
    </div>
    </PayPalScriptProvider>
  );
}

export default App;
