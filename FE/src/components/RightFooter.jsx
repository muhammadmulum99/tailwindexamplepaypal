import React from "react";

const RightFooter = () => {
  return (
    <div className="flex justify-between">
      <div className="pl-0">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-6 w-6"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fill-rule="evenodd"
            d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
            clip-rule="evenodd"
          />
        </svg>
      </div>
      <button className="absolute bottom-0 right-0 py-8 px-16 bg-[#FFE192] font-semibold text-2xl hover:bg-yellow-300">
        Add to cart
      </button>
    </div>
  );
};

export default RightFooter;
