import React from 'react'

function HeroTextContent() {
  return (
    <div className='flex flex-col gap-8 items-start w-[80%]'>
      <h1 className='text-7xl font-bold'>Wood Candy Sofa</h1>
      <p className='text-[#999999]'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque ipsam hic saepe corrupti nihil, laboriosam corporis aliquid doloribus, aut, fugiat rem.</p>
      <strong className='text-2xl font-semibold'>$300.00</strong>
      <button className='bg-[#FFE8A9] px-8 py-2 hover:bg-amber-400'>Buy Now</button>
    </div>
  )
}

export default HeroTextContent
