import React from 'react'

const NavLinks = () => {
  return (
    <div className='flex gap-32 justify-between text-[#999999]'>
      <a href="#" className='py-9'>Types</a>
      <a href="#"  className='py-9 hover:bg-[#FFE8A9] hover:px-16 hover:font-semibold hover:text-[#343434]'>Price</a>
      <a href="#"  className='py-9'>Connect</a>
    </div>
  )
}

export default NavLinks
